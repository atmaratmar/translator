import "./App.css";
import {
  BrowserRouter,
  Switch,
  Route,
  // Redirect
} from "react-router-dom";
import AppContainer from "./hoc/AppContainer";
import Login from "./components/Login/Login";
import Translator from "./components/Translator/Translator";
import Register from "./components/Register/Register";
import NotFound from "./components/NotFound/NotFound";
import Profile from "./components/Profile/Profile";


function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <AppContainer>
          <h1>American Sign Language Alphabet </h1>
          <h4>
            {" "}
          </h4>
        </AppContainer>
        <Switch>
          <Route path="/" exact component={Login} />
          <Route path="/register" component={Register} />
          <Route path="/translator" component={Translator} />
          <Route path="/profile" component={Profile} />
          <Route path="*" component={NotFound}/>
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
