function Register(users) {
  return fetch("http://localhost:8000/users", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(users),
  }).then(async (response) => {
    if (!response.ok) {
      const { error } = await response.json();
      throw new Error(error);
    }
   // console.log(users);
    return response.json();
  });
}
