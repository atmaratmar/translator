import { useDispatch } from "react-redux";
import AppContainer from "../../hoc/AppContainer";
import { Link } from "react-router-dom";
import { useState } from "react";
import { registerSuccessAction } from "../store/actions/RegisterAction";
const Register = () => {
  const dispatch = useDispatch();
  // const {loggedin}=useSelector(state =>state.sessionReducer)
  const [users, setCredentials] = useState({
    username: "",
    id: "",
    translations: []
  });
////////////////////////////////////////////
  function Register(users) {
    return fetch("http://localhost:8000/users", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(users),
    }).then(async (response) => {
      if (!response.ok) {
        const { error } = await response.json();
        throw new Error(error);
      }
     // console.log(users);
      return response.json();
    });
  }
///////////////////////////////////////////
  const onInputChange = (event) => {
    setCredentials({
      ...users,
      [event.target.id]: event.target.value,
    });
  };
  const onRegisterSubmit = (event) => {
    event.preventDefault();
    Register(users);
    console.log(users);
    dispatch(registerSuccessAction(users));
  };

  return (
    <AppContainer>
      {/* {loggedin && <Redirect to="/Translator"/>} */}
      <form onSubmit={onRegisterSubmit}>
        <h1> Register for Dictionary</h1>
        <div className="mb3">
          <label htmlFor="username" className="form-lable">
            User Name *
          </label>
          <input
            onChange={onInputChange}
            type="text"
            id="username"
            className="form-control"
            placeholder="Name"
          ></input>
        </div>
        <button className="btn btn-success btn-lg">Sing UP</button>
      </form>
      <p>Already Registerd</p>
      <Link to="/">  Login here</Link>
    </AppContainer>
  );
};

export default Register;
