import Signs from './AmericanSign'
import {useState} from "react";
import NavBar from '../NavBar/NavBar';
import { Redirect } from 'react-router-dom';

function Translate() {
    const [redirect, setRedirect]= useState(false) 
    const [text, setText] = useState('')
    const [translationText, setTranslationText] = useState('')
    const onInputChange = (e) => setText(e.target.value)
    const handleTranslation = () => {
    
        
        // Update localStorage
        let translations = JSON.parse(localStorage.getItem('Search-History'))
             console.log(translations)
             if(translations){
                if (translations.length < 10) {
                    translations.unshift(text)
                } else {
                    translations.pop()
                    translations.unshift(text)
                }

             }else{
                setRedirect({ redirect: true })
             }
         
        localStorage.setItem('Search-History', JSON.stringify(translations))
        //Remove anything that is not an English character or a whitespace
        const regex = /[^a-zA-Z\s]/g
        const cleanedText = text.replace(regex, '')
        // Set the text to translate
        setTranslationText(cleanedText)  
        const {id} = JSON.parse(localStorage.getItem('rtxtf'))
        Myhistory(id)   
    }
    const onFormSubmit = event=>{
        event.preventDefault()      
        
    }
//////////////////////////////////////////////
const history =  JSON.parse(localStorage.getItem('Search-History'))
 

async function  Myhistory(id){
    if(history){

        return await fetch('http://localhost:8000/users/'+id,{
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ translations: history})
        }).then(r => r.json())
        }
        else{
            setRedirect({ redirect: true }) 
        }
    }
   
   
//////////////////////////////////////////////777  
    return (
        <div>
            <NavBar></NavBar>
            <div className="container" onSubmit={ onFormSubmit}>
            <label htmlFor="text"> Enter text</label>
            <input type="text"  onChange={onInputChange} maxLength="40" placeholder="Enter text" />           
            <button className="btn" onClick={handleTranslation}  type="button" >Translate</button>
            {translationText.length > 0 && <Signs sentence={translationText}/>}  
            { redirect ? (<Redirect push to="/"/>) : null }        
           
        </div>
        </div>
      
    )
}

export default Translate