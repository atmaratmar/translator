
function Signs({sentence}) {
    const letters = sentence.toLowerCase().split('')

    return (
        <div>
            {letters.map((letter, index) =>
            letter === ' '
            ? <span key={index}>&nbsp;</span>: 
            <img key={index} src={`${process.env.PUBLIC_URL}/AMC_SIGN/${letter}.png`} alt={letter} />
            )}    
        </div>
    )
}

export default Signs