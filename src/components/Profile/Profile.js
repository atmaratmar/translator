
import { Link } from "react-router-dom"
import Logout from "../Logout/Logout"
const Profile = () => {
 let userDetails = JSON.parse(localStorage.getItem('rtxtf'))
 const{username} =userDetails
 const history =  JSON.parse(localStorage.getItem('Search-History'))
  return (
    <div className="container">
      <Logout></Logout>
      <br></br>
      <button  > <Link to="/" >back</Link></button>
      <h1>{username}</h1>
      <br></br>
      <h2>Search History</h2>
      {history.map(history=><li key={history.id}>{history}</li>)}
    </div>
  );
};
export default Profile;