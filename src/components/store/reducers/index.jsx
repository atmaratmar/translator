import { combineReducers } from "redux";
import { loginReducer } from "./LoginReducer";
import { sessionReducer } from "./sessionReducer";
import { registerReducer } from "./RegisterReducer";

const appReducer = combineReducers({
  loginReducer,
  sessionReducer,
  registerReducer,
});
export default appReducer;
