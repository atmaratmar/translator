import {
  ACTION_LOGIN_ATTEMPTING,
  ACTION_LOGIN_ERORR,
  ACTION_LOGIN_SUCCESS,
} from "../actions/LoginAction";

const initialState = {
  logingAttempting: false,
  loginErorr: "",
};

export const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_LOGIN_ATTEMPTING:
      return {
        ...state,
        logingAttempting: true,
        loginErorr: "",
      };

    case ACTION_LOGIN_SUCCESS:
      return {
        ...initialState,
      };
    case ACTION_LOGIN_ERORR:
      return {
        ...state,
        logingAttempting: false,
        loginErorr: action.payload,
      };

    default:
      return state;
  }
};
