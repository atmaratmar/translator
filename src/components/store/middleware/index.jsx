import { applyMiddleware } from "redux";
import { loginMiddleware } from "./LoginMiddleware";
import { sessionMidleware } from "./sessionMiddleware";

export default applyMiddleware(loginMiddleware, sessionMidleware);
