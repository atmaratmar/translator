import { RegisterAPI } from "../../Register/RegisterAPI";
import { ACTION_REGISTER_SUCCESS } from "../actions/RegisterAction";
export const registerMiddleware = ({ dispatch }) => (next) => (action) => {
  next(action);

  if (action.type === ACTION_REGISTER_SUCCESS) {
    RegisterAPI.Register(action.payload).then((profile) => {
      dispatch(registerSuccessAction(profile));
    });
  }
};
