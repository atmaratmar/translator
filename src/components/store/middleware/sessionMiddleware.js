import {
  ACTION_SESSION_INIT,
  ACTION_SESSION_SET,
  sessionSetAction,
} from "../actions/sessionAction";
export const sessionMidleware = ({ dispatch }) => (next) => (action) => {
  next(action);

  if (action.type === ACTION_SESSION_INIT) {
    const storedSession = localStorage.getItem("rtxtf");
    if (!storedSession) {
      return;
    }
    const session = JSON.parse(storedSession);
    dispatch(sessionSetAction(session));
  }

  if (action.type === ACTION_SESSION_SET) {
    //  const {username}=action.payload
    // localStorage.setItem('rtxtf',JSON.stringify(username))
    const { translations } = action.payload;
    localStorage.setItem("Search-History", JSON.stringify(translations));
    localStorage.setItem("rtxtf", JSON.stringify(action.payload));
  }
};
