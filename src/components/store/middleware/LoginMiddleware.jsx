import { LoginAPI } from "../../Login/LoginAPI";
import {
  ACTION_LOGIN_ATTEMPTING,
  ACTION_LOGIN_SUCCESS,
  loginErorrAction,
  loginSuccessAction,
} from "../actions/LoginAction";
import { sessionSetAction } from "../actions/sessionAction";

export const loginMiddleware = ({ dispatch }) => (next) => (action) => {
  next(action);

  if (action.type === ACTION_LOGIN_ATTEMPTING) {
    LoginAPI.login(action.payload)
      .then((profile) => {
        dispatch(loginSuccessAction(profile));
      })
      .catch((error) => {
        dispatch(loginErorrAction(error.message));
      });
  }
  if (action.type === ACTION_LOGIN_SUCCESS) {
    dispatch(sessionSetAction(action.payload));
  }
};
