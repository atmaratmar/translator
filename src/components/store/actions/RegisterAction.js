export const ACTION_REGISTER_SUCCESS = "[register] SUCCESS";
export const registerSuccessAction = (users) => ({
  type: ACTION_REGISTER_SUCCESS,
  payload: users,
});
