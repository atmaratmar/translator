export const ACTION_LOGIN_ATTEMPTING = "[login] ATTEMPT";
export const ACTION_LOGIN_SUCCESS = "[login] SUCCESS";
export const ACTION_LOGIN_ERORR = "[login] ERORR";

export const loginAttemtAction = (credensials) => ({
  type: ACTION_LOGIN_ATTEMPTING,
  payload: credensials,
});
export const loginSuccessAction = (profile) => ({
  type: ACTION_LOGIN_SUCCESS,
  payload: profile,
});
export const loginErorrAction = (erorr) => ({
  type: ACTION_LOGIN_ERORR,
  payload: erorr,
});
