import { useState } from "react";
import { Redirect } from 'react-router-dom';

const Logout = () => { 
    const [redirect, setRedirect]= useState(false) 
    const onFormSubmit = ()=>{  

         localStorage.clear()
         setRedirect({ redirect: true })     
    }
   
    return (    
         <div>           
            <button className="btn btn-primary btn-lg" onClick={onFormSubmit} >
            logout
            { redirect ? (<Redirect push to="/opps"/>) : null }
           </button>
        
       

           
    </div>
     )
} 
export default Logout;