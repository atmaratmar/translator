import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, Redirect } from "react-router-dom";
import AppContainer from "../../hoc/AppContainer";
import { loginAttemtAction } from "../store/actions/LoginAction";
const Login = () => {
  const dispatch = useDispatch();
  const { loggedin } = useSelector((state) => state.sessionReducer);
  const [credentials, setCredentials] = useState({
    username: "",
  });
  const onInputChange = (event) => {
    setCredentials({
      ...credentials,
      [event.target.id]: event.target.value,
    });
  };
  const onFormSubmit = (event) => {
    event.preventDefault();
    dispatch(loginAttemtAction(credentials));
  };
  return (
    <>
      {loggedin && <Redirect to="/Translator" />}
      {!loggedin && (
        <AppContainer>
          <form className="mt-3" onSubmit={onFormSubmit}>
            <h1>Login Translator</h1>
            <div className="mb-3">
              <label htmlFor="username" className="form-label">
                Username
              </label>
              <input
                id="username"
                type="text"
                placeholder="Enter your User Name"
                className="form-control"
                onChange={onInputChange}
              />
            </div>
            <button type="submit" className="btn btn-primary btn-lg">
              Login{" "}
            </button>
          </form>
          <p>If not register please register first</p>
          <Link to="/Register"> Register</Link>
        </AppContainer>
      )}
    </>
  );
};
export default Login;
